﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WikiDataBaseContext.DataAccessLayer;
using System.IO;
using WikiModel.Model;

namespace WIKIPRJ.Controllers
{
    public class DocumentsController : Infrastructure.BaseController
    {
        private DataBaseContext db = new DataBaseContext();
        private string UploadFilePath = "~/DocFiles";
        // GET: Documents
        public ActionResult Index()
        {
            var documents = db.Documents.Include(d => d.Group).Include(d => d.signatory).Include(d => d.User);
            return View(documents.ToList());
        }

        // GET: Documents/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document document = db.Documents.Find(id);
            if (document == null)
            {
                return HttpNotFound();
            }
            return View(document);
        }
        // GET: Documents/Context/5
        public ActionResult Context(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document document = db.Documents.Include(d => d.Attachments).Where(d => d.ID == id).FirstOrDefault();
            //IList<Attachment> att = document.Attachments.ToList();
            if (document == null)
            {
                return HttpNotFound();
            }
            return View(document);
        }

        // GET: Documents/Create
        public ActionResult Create()
        {
            ViewBag.GroupID = new SelectList(db.Groups, "ID", "DisplayGroupName");
            ViewBag.SignatoryID = new SelectList(db.Signatoris, "ID", "FullName");
            ViewBag.UserID = new SelectList(db.Users, "ID", "FullName");
            return View();
        }

        // POST: Documents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,GroupID,Title,KeyWord,Context,Date_Doc,Date_Register,UserID,SignatoryID")] Document document)
        {
            //ModelState.Clear();
            //TryValidateModel(document);
            if (ModelState.IsValid)
            {
                db.Documents.Add(document);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GroupID = new SelectList(db.Groups, "ID", "DisplayGroupName", document.GroupID);
            ViewBag.SignatoryID = new SelectList(db.Signatoris, "ID", "FullName", document.SignatoryID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "FullName", document.UserID);
            return View(document);
        }

        // GET: Documents/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document document = db.Documents.Find(id);
            if (document == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupID = new SelectList(db.Groups, "ID", "DisplayGroupName", document.GroupID);
            ViewBag.SignatoryID = new SelectList(db.Signatoris, "ID", "FullName", document.SignatoryID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "FullName", document.UserID);
            return View(document);
        }

        // POST: Documents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,GroupID,Title,KeyWord,Context,Date_Doc,Date_Register,UserID,SignatoryID")] Document document)
        {
            if (ModelState.IsValid)
            {
                db.Entry(document).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GroupID = new SelectList(db.Groups, "ID", "DisplayGroupName", document.GroupID);
            ViewBag.SignatoryID = new SelectList(db.Signatoris, "ID", "FullName", document.SignatoryID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "FullName", document.UserID);
            return View(document);
        }

        // GET: Documents/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document document = db.Documents.Find(id);
            if (document == null)
            {
                return HttpNotFound();
            }
            return View(document);
        }

        // POST: Documents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            //Document document = db.Documents.Find(id);
            Document document = db.Documents.Include(d => d.Attachments).Where(d => d.ID == id).FirstOrDefault();
            if (document != null)
            {
                foreach (var files in document.Attachments.ToList())
                {
                    var path = Path.Combine(Server.MapPath(files.AttPath), files.AttName);
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                }
                    db.Documents.Remove(document);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult Download(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attachment attachment = db.Attachments.Find(id);
            if (attachment == null)
            {
                return HttpNotFound();
            }

            string path = Path.Combine(Server.MapPath(UploadFilePath), attachment.AttName);
            switch (attachment.AttType.ToLower())
            {
                case ".jpg":
                    return (File(path, contentType: "Image/jpeg", fileDownloadName: attachment.AttName));
                default:
                    return (File(path, contentType: "harchi/harchi", fileDownloadName: attachment.AttName));
            }

            //return View(attachment);
        }
        public ActionResult AboutMe()
        {
            return View();
        }
    }
}
