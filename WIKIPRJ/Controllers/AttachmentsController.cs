﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WikiDataBaseContext.DataAccessLayer;
using WikiModel.Model;
using System.IO;
namespace WIKIPRJ.Controllers
{
    public class AttachmentsController : Infrastructure.BaseController
    {
        private DataBaseContext db = new DataBaseContext();
        private string UploadFilePath = "~/DocFiles";

        // GET: Attachments
        public ActionResult Index()
        {
            var attachments = db.Attachments.Include(a => a.Document);
            return View(attachments.ToList());
        }

        // GET: Attachments/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attachment attachment = db.Attachments.Find(id);
            if (attachment == null)
            {
                return HttpNotFound();
            }
            return View(attachment);
        }

        // GET: Attachments/Create
        public ActionResult Create(long? id)
        {
            ViewBag.DocumentID = id;//new SelectList(db.Documents, "ID", "Title");
            return View();
        }

        // POST: Attachments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,AttTitle")] Attachment attachment, HttpPostedFileBase files,long id)
        {
            string path = Path.Combine(Server.MapPath(UploadFilePath), Path.GetFileName(files.FileName));
            files.SaveAs(path);
            attachment.AttName = Path.GetFileName(files.FileName);
            attachment.AttPath = UploadFilePath;
            attachment.AttSize = files.ContentLength;
            attachment.AttType = Path.GetExtension(files.FileName);
            attachment.Date_Register = MyClasses.PersianDate.Now();
            attachment.DocumentID = id;
            ModelState.Clear();
            TryValidateModel(attachment);
            if (ModelState.IsValid)
            {

                db.Attachments.Add(attachment);
                db.SaveChanges();
                return RedirectToAction("Index","Documents");
            }

            //ViewBag.DocumentID = new SelectList(db.Documents, "ID", "Title", attachment.DocumentID);
            return View(attachment);
        }

        // GET: Attachments/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attachment attachment = db.Attachments.Find(id);
            if (attachment == null)
            {
                return HttpNotFound();
            }
            ViewBag.DocumentID = new SelectList(db.Documents, "ID", "Title", attachment.DocumentID);
            return View(attachment);
        }

        // POST: Attachments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,AttName,AttPath,AttSize,AttType,Date_Register,DocumentID")] Attachment attachment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attachment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DocumentID = new SelectList(db.Documents, "ID", "Title", attachment.DocumentID);
            return View(attachment);
        }

        // GET: Attachments/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attachment attachment = db.Attachments.Find(id);
            if (attachment == null)
            {
                return HttpNotFound();
            }
            return View(attachment);
        }

        // POST: Attachments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Attachment attachment = db.Attachments.Find(id);
            if (attachment != null)
            {
                var path = Path.Combine(Server.MapPath(attachment.AttPath), attachment.AttName);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                db.Attachments.Remove(attachment);
                db.SaveChanges();
            }

            return RedirectToAction("Index","Documents");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult Save(IEnumerable<HttpPostedFileBase> AttName)
        {               
                // ...
                // Process the files and save them
                // ...

            // Return an empty string to signify success
            return Content("");
        }

        [HttpPost]
        public ContentResult Remove(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    // ...
                    // delete the files
                    // ...
                }
            }

            // Return an empty string to signify success
            return Content("");
        }
     


    }
}

