﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WikiDataBaseContext.DataAccessLayer;
using WikiModel.Model;
using WIKIPRJ.ViewModels;
using System.Data;
using System.Data.Entity;

namespace WIKIPRJ.Controllers
{
    public class HomeController : Infrastructure.BaseController
    {
        // GET: Home
        private DataBaseContext db = new DataBaseContext();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]

        public ActionResult Delete(int? id)
        {
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            return View();
        }
        [HttpGet]
        public ActionResult Index(string sort,string sortdir,int? page=1)
        {
            var list = db.Groups.Include(d=>d.Documents).ToList();
            return View(list);
        }

    }
}