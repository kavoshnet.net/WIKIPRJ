﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WikiDataBaseContext.DataAccessLayer;
using WikiModel.Model;
using WIKIPRJ.ViewModels;

namespace WIKIPRJ.Controllers
{
    public class GroupsController : Infrastructure.BaseController
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Groupss
        public ActionResult Index()
        {
            return View(db.Groups.ToList());
        }

        // GET: Groupss/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group Groups = db.Groups.Find(id);
            if (Groups == null)
            {
                return HttpNotFound();
            }
            return View(Groups);
        }

        // GET: Groupss/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Groupss/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ParentID,GroupName,HasChildren")] Group Groups)
        {
            if (ModelState.IsValid)
            {
                Groups.HasChildren = false;
                Groups.ParentID = 0;
                db.Groups.Add(Groups);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(Groups);
        }
        // GET: Groupss/Create
        public ActionResult Append(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group Groups = db.Groups.Find(id);
            if (Groups == null)
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                db.Entry(Groups).State = EntityState.Modified;
                Groups.HasChildren = true;
                db.SaveChanges();
            }
            return View(Groups);
        }

        // POST: Groupss/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Append([Bind(Include = "ID,ParentID,GroupName,HasChildren")] Group Groups)
        {
            if (ModelState.IsValid)
            {
                Groups.ParentID = Groups.ID;
                db.Groups.Add(Groups);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(Groups);
        }

        // GET: Groupss/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group Groups = db.Groups.Find(id);
            if (Groups == null)
            {
                return HttpNotFound();
            }
            return View(Groups);
        }

        // POST: Groupss/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ParentID,GroupName,HasChildren")] Group Groups)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Groups).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Groups);
        }

        // GET: Groupss/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group Groups = db.Groups.Find(id);
            if (Groups == null)
            {
                return HttpNotFound();
            }
            return View(Groups);
        }

        // POST: Groupss/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Group Groups = db.Groups.Find(id);
            db.Groups.Remove(Groups);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult GetData(int? id)
        {
            if (id == null)
            {
                //دریافت ریشه‌ها
                return Json(
                    db.Groups.Where(x => x.ParentID == 0) // ریشه‌ها
                        .ToList(),
                    JsonRequestBehavior.AllowGet);
            }
            else
            {
                //دریافت فرزندهای یک ریشه
                return Json(
                    db.Groups
                              .Where(x => x.ParentID == id)
                              .ToList(),
                              JsonRequestBehavior.AllowGet);
            }
        }


        private string serialized;
        private Dictionary<int, int> numbers = new Dictionary<int, int>();
        public ActionResult SaveMenu(string serial)
        {
            serialized = serial;
            calculte_serialized(0);
            foreach (var item in numbers)
                db.Groups.Where(x => x.ID == item.Key).First().ParentID = item.Value;
            db.SaveChanges();
            db.Groups.ToList().ForEach(a => a.HasChildren = false);
            db.SaveChanges();
            //انتخاب تمام پرنت ای دی ها از جدول
            var PIDList = db.Groups.Select(x => x.ParentID).Distinct().ToArray();
            //تغییر تمام ردیفهایی که فرزند دارند
            db.Groups.Where(x => PIDList.Contains(x.ID)).ToList().ForEach(a => a.HasChildren = true);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        void calculte_serialized(int parent)
        {
            while (serialized.Length > 0)
            {
                var id_index = serialized.IndexOf("id");
                if (id_index == -1)
                {
                    return;
                }
                serialized = serialized.Substring(id_index + 5);
                var quote_index = serialized.IndexOf("\"");
                var id = serialized.Substring(0, quote_index);
                numbers.Add(int.Parse(id), parent);
                serialized = serialized.Substring(quote_index);
                var condition = serialized.Substring(0, 3);
                switch (condition)
                {
                    case "\"},":
                        break;
                    case "\",\"":
                        calculte_serialized(int.Parse(id));
                        break;
                    case "\"}]":
                        return;
                        break;
                    default:
                        break;
                }
            }
        }
        public ActionResult GroupDocuments(long ?id)
        {
//            if (id != null)
            {
                var Documents = db.Documents.Where(p=>p.GroupID == id).OrderBy(p=>p.Date_Doc).ToList();
                var Groups = db.Groups.ToList();
                var model = new GroupDocuments
                {
                    Documents = Documents,
                    Groups = Groups
                };
                return View(model);
            }
        //    else
        //        return View(db.Groups);
        }
        public ActionResult SearchDocuments(string sort, string sortdir, int? page = 1, string SearchDoc="")
        {
            var Documents = db.Documents.Where(p => p.Title.Contains(SearchDoc) || p.KeyWord.Contains(SearchDoc) || p.Context.Contains(SearchDoc)).OrderBy(p => p.Date_Doc).ToList();
            var Groups = db.Groups.ToList();
            var model = new GroupDocuments
            {
                Documents = Documents,
                Groups = Groups
            };
            return View(model);

        }
    }
}
