﻿using System.Web;
using System.Web.Optimization;

namespace WIKIPRJ
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-2.2.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-2.6.2.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-rtl").Include(
                      "~/Scripts/bootstrap.rtl.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Site.css",
                      "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/css-rtl").Include(
                      "~/Content/Site.css",
                      "~/Content/bootstrap.rtl.css"));

            bundles.Add(new StyleBundle("~/Scripts/ckeditor").Include(
                      "~/Scripts/ckeditor/adapters/jquery.js",
                      "~/Scripts/ckeditor/ckeditor.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/kendocss").Include(
                      "~/Content/kendo.common.min.css",
                      "~/Content/kendo.default.min.css"));

            bundles.Add(new StyleBundle("~/Content/kendocss-rtl").Include(
                      "~/Content/kendo.common.min.css",
                      "~/Content/kendo.rtl.min.css",
                      "~/Content/kendo.default.min.css"));

            bundles.Add(new StyleBundle("~/Content/kendoscr").Include(
                      "~/Scripts/jquery-2.2.1.min.js",
                      "~/Scripts/kendo.all.min.js",
                      "~/Scripts/cultures/moment.min.js",
                      "~/Scripts/cultures/moment-jalaali.js",
                      "~/Scripts/messages/kendo.messages.en-US.js",
                      "~/Scripts/jquery.validate.min.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js",
                      "~/Scripts/jquery.unobtrusive-ajax.min.js",
                      "~/Scripts/kendo.aspnetmvc.js"
                      ));

            bundles.Add(new StyleBundle("~/Scripts/kendoscr-rtl").Include(
                       "~/Scripts/jquery-2.2.1.min.js", 
                       "~/Scripts/kendo.all.min.js",
                       "~/Scripts/cultures/kendo.culture.fa-IR.js",
                       "~/Scripts/cultures/kendo.culture.fa.js",
                       "~/Scripts/messages/kendo.messages.en-US.js",
                       "~/Scripts/cultures/moment.min.js",
                       "~/Scripts/cultures/moment-jalaali.js",
                      "~/Scripts/messages/kendo.fa-IR.js",
                      "~/Scripts/jquery.validate.min.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js",
                      "~/Scripts/jquery.unobtrusive-ajax.min.js",
                      "~/Scripts/kendo.aspnetmvc.js"));

        }
    }
}
