﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WikiModel.Model;

namespace WIKIPRJ.ViewModels
{
    public class GroupDocuments
    {
        public GroupDocuments()
        {

        }
        public IList<Group> Groups { get; set; }
        public IList<Document> Documents { get; set; }
    }
}