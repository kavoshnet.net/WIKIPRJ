﻿using System;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WIKIPRJ.Helpers;

namespace WIKIPRJ.Infrastructure
{
    /// <summary>
    /// کلاس بیس کنترلر
    /// در این کلاس زبان سایت را اول کار مشخص میکنیم
    /// </summary>
    public class BaseController : Controller
    {

        //#################################################

        public BaseController()
        {
            //System.Globalization.CultureInfo oCultureInfo =
            //    new System.Globalization.CultureInfo("en-US");

            System.Globalization.CultureInfo oCultureInfo =
                new System.Globalization.CultureInfo("fa");

            //System.Globalization.CultureInfo oCultureInfo =
            //	new System.Globalization.CultureInfo("fr-FR");

            System.Threading.Thread.CurrentThread.CurrentCulture = oCultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = oCultureInfo;
        }

        //#################################################

        //برای چنج کالچر و سشن  دقیقا مثل ای اس پی دات نت وب فرم میتوان در این صفحه عمل نمود
        //    public BaseController()
        //    {

        //    }
        //    private const string LanguageCookieName = "MyLanguageCookieName";
        //    protected override void ExecuteCore()
        //    {
        //        var cookie = HttpContext.Request.Cookies[LanguageCookieName];
        //        string lang;
        //        if (cookie != null)
        //        {
        //            lang = cookie.Value;
        //        }
        //        else
        //        {
        //            lang = ConfigurationManager.AppSettings["DefaultCulture"] ?? "fa-IR";
        //            var httpCookie = new HttpCookie(LanguageCookieName, lang) { Expires = DateTime.Now.AddYears(1) };
        //            HttpContext.Response.SetCookie(httpCookie);
        //        }
        //        Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(lang);
        //        base.ExecuteCore();
        //    }

        //protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        //{
        //    string cultureName = null;

        //    // Attempt to read the culture cookie from Request
        //    HttpCookie cultureCookie = Request.Cookies["_culture"];
        //    if (cultureCookie != null)
        //        cultureName = cultureCookie.Value;
        //    else
        //        cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ? Request.UserLanguages[0] : null; // obtain it from HTTP header AcceptLanguages

        //    // Validate culture name
        //    cultureName = "fa";
        //    cultureName = WIKIPRJ.Helpers.CultureHelper.GetImplementedCulture(cultureName); // This is safe


        //    // Modify current thread's cultures            
        //    Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
        //    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;


        //    return base.BeginExecuteCore(callback, state);
        //}


    }
}