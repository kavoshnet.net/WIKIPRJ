﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace WikiDataBaseContext.DataAccessLayer
{
    //class DataBaseContextInitializer : DropCreateDatabaseIfModelChanges<DataBaseContext>
    //class DataBaseContextInitializer : DropCreateDatabaseIfModelChanges<DataBaseContext>
    class DataBaseContextInitializer : MigrateDatabaseToLatestVersion<DataAccessLayer.DataBaseContext,Migrations.Configuration>
    {
        public DataBaseContextInitializer()
        {

        }
        //protected override void Seed(DataBaseContext context)
        //{
        //    base.Seed(context);
        //    context.Groups.Add(new WikiModel.Model.Group { ParentID = 0, GroupName = "قانون اساسی" });
        //    context.Groups.Add(new WikiModel.Model.Group { ParentID = 0, GroupName = "قانون مدنی" });
        //    context.Groups.Add(new WikiModel.Model.Group { ParentID = 0, GroupName = "قانون ثبت احوال" });
        //    context.Groups.Add(new WikiModel.Model.Group { ParentID = 0, GroupName = "بخشنامه های اداری" });
        //    context.Groups.Add(new WikiModel.Model.Group { ParentID = 3, GroupName = "قوانین حقوقی" });
        //    context.Groups.Add(new WikiModel.Model.Group { ParentID = 5, GroupName = "قوانین سجلی" });
        //    context.Groups.Add(new WikiModel.Model.Group { ParentID = 6, GroupName = "قوانین تغییر سن" });
        //    context.Groups.Add(new WikiModel.Model.Group { ParentID = 6, GroupName = "قوانین تغیر نام" });
        //    context.Groups.Add(new WikiModel.Model.Group { ParentID = 6, GroupName = "قوانین تغیر نام خانوادگی" });

        //    context.Users.Add(new WikiModel.Model.User { Fname="مهدی",Lname="اخلاص",UserName="admin",Password="admin",IsAct=true});

        //    context.Signatoris.Add(new WikiModel.Model.Signatory { Fname = "مهدی", Lname = "اخلاص", Occupation = "مدیر کل", IsAct = true });
        //}
    }
}
