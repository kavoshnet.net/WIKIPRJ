﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using WikiModel.Model;
namespace WikiDataBaseContext.DataAccessLayer
{
    public class DataBaseContext:DbContext
    {
        public DataBaseContext():base("WikiDB")
        {
            Configuration.LazyLoadingEnabled = true;
        }
        static DataBaseContext()
        {
            Database.SetInitializer(new DataBaseContextInitializer());
        }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Signatory> Signatoris { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
    }
}
