﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;
using WIKIPRJ.Resources;
namespace WikiModel.Model
{
    public class Group
    {
        private int MessageSize = 30;
        public Group()
        {

        }
        [Required(ErrorMessageResourceName = "IDGroup_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "IDGroup", ResourceType = typeof(Caption))]
        public Int64 ID { get; set; }

        [Required(ErrorMessageResourceName = "ParentID_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "ParentID", ResourceType = typeof(Caption))]
        public Int64 ParentID { get; set; }

        [Required(ErrorMessageResourceName = "GroupName_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "GroupName", ResourceType = typeof(Caption))]
        public string GroupName { get; set; }

        [Required(ErrorMessageResourceName = "HasChildren_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "HasChildren", ResourceType = typeof(Caption))]
        [DefaultValue(false)]
        public bool HasChildren { get; set; }
        //[Required(ErrorMessageResourceName = "Documents", ErrorMessageResourceType = typeof(Texts))]
        //[Display(Name = "Documents", ResourceType = typeof(Texts))]
        
        //ارتباط جداول
        public IList<Document> Documents { get; set; }

        //نحوه نمایش
        public string DisplayGroupName
        {
            get
            {
                if (this.GroupName.Length > this.MessageSize)
                    return this.GroupName.Substring(0, this.MessageSize) + "...";
                else
                    return this.GroupName;
            }
        }

        //public virtual Group Parent { get; set; }
        //public virtual ICollection<Group> Children { get; set; }

        //private readonly IList<Group> _group;
        //public IEnumerable<Group> getgroup
        //{
        //    get
        //    {
        //        return new SelectList()
        //    }
        //}
        //public IEnumerable<Group> getgroup()
        //{
        //    var db = new Group();
        //    IEnumerable<Group> x = new List<Group>();
        //    var test = (from name in Group.  select name).ToList();
        //    x = test;
        //    return x;
        //}
    }
}