﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
using WIKIPRJ.Resources;

namespace WikiModel.Model
{
    public class User
    {
        public User()
        {

        }
        [Required(ErrorMessageResourceName = "IDUser_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "IDUser", ResourceType = typeof(Caption))]
        public Int64 ID { get; set; }

        [Required(ErrorMessageResourceName = "Fname_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "Fname", ResourceType = typeof(Caption))]
        public string Fname { get; set; }

        [Required(ErrorMessageResourceName = "Lname_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "Lname", ResourceType = typeof(Caption))]
        public string Lname { get; set; }


        [Required(ErrorMessageResourceName = "UserName_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "UserName", ResourceType = typeof(Caption))]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceName = "Password_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "Password", ResourceType = typeof(Caption))]
        public string Password { get; set; }

        [Required(ErrorMessageResourceName = "IsAct_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "IsAct", ResourceType = typeof(Caption))]
        public bool IsAct { get; set; }

        //[Required(ErrorMessageResourceName = "Documents", ErrorMessageResourceType = typeof(Texts))]
        //[Display(Name = "Documents", ResourceType = typeof(Texts))]
        
        //ارتباط جداول
        public IList<Document> Documents { get; set; }
        public string FullName {
            get
            {
                return MyClasses.Util.SubString(Fname + " " + Lname,80);
            }
        }

    }
}
