﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
using WIKIPRJ.Resources;

namespace WikiModel.Model
{
    public class Document
    {
        private int MessageSize = 40;
        public Document()
        {
            Date_Register = MyClasses.PersianDate.Now();
            UserID = 1;
            SignatoryID = 1;
        }
        [Required(ErrorMessageResourceName = "IDDocument_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "IDDocument", ResourceType = typeof(Caption))]
        public Int64 ID { get; set; }

        [Required(ErrorMessageResourceName = "Title_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "Title", ResourceType = typeof(Caption))]
        public string Title { get; set; }

        [Required(ErrorMessageResourceName = "KeyWord_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "KeyWord", ResourceType = typeof(Caption))]
        public string KeyWord { get; set; }


        [Required(ErrorMessageResourceName = "Context_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "Context", ResourceType = typeof(Caption))]
        public string Context { get; set; }

        [Required(ErrorMessageResourceName = "Date_Doc_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "Date_Doc", ResourceType = typeof(Caption))]
        public string Date_Doc { get; set; }

        //[Required(ErrorMessageResourceName = "Date_Register_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "Date_Register", ResourceType = typeof(Caption))]
        public string Date_Register { get; set; }


        //ارتباط جداول
        [Required(ErrorMessageResourceName = "IDGroup_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "GroupID", ResourceType = typeof(Caption))]
        public Int64 GroupID { get; set; }

        //[Required(ErrorMessageResourceName = "IDUser_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "UserID", ResourceType = typeof(Caption))]
        public Int64 UserID { get; set; }

        //[Required(ErrorMessageResourceName = "IDSignatory_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "SignatoryID", ResourceType = typeof(Caption))]
        public Int64 SignatoryID { get; set; }
        public virtual Group Group { get; set; }
        public virtual User User { get; set; }
        public virtual Signatory signatory { get; set; }
        public IList<Attachment> Attachments { get; set; }



        //public string DisplayTitle
        //{
        //    get
        //    {
        //        string res = MyClasses.Util.SubString(Title, 50);
        //        return res;
        //    }
        //}

        public string DisplayTitle
        {
            get
            {
                if (this.Title.Length > this.MessageSize)
                    return this.Title.Substring(0, this.MessageSize) + "...";
                else
                    return this.Title;
            }
        }


        public string DisplayKeyWord
        {
            get
            {
                if (this.KeyWord.Length > this.MessageSize)
                    return this.KeyWord.Substring(0, this.MessageSize) + "...";
                else
                    return this.KeyWord;
            }
        }

        public string DisplayContext
        {
            get
            {
                if (this.Context.Length > this.MessageSize)
                    return this.Context.Substring(0, this.MessageSize) + "...";
                else
                    return this.Context;
            }
        }
    }
}
