﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WIKIPRJ.Resources;
namespace WikiModel.Model
{
    public class Attachment
    {
        public Attachment()
        {

        }
        [Required(ErrorMessageResourceName = "IDAtt_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "IDAtt", ResourceType = typeof(Caption))]
        public Int64 ID { get; set; }

        [Required(ErrorMessageResourceName = "AttTitle_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "AttTitle", ResourceType = typeof(Caption))]
        public string AttTitle { get; set; }

        [Required(ErrorMessageResourceName = "AttName_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "AttName", ResourceType = typeof(Caption))]
        public string AttName { get; set; }

        [Required(ErrorMessageResourceName = "AttPath_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "AttPath", ResourceType = typeof(Caption))]
        public string AttPath { get; set; }

        [Required(ErrorMessageResourceName = "AttSize_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "AttSize", ResourceType = typeof(Caption))]
        public Int64 AttSize { get; set; }


        [Required(ErrorMessageResourceName = "AttType_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "AttType", ResourceType = typeof(Caption))]
        public string AttType { get; set; }

        [Required(ErrorMessageResourceName = "Date_Register_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "Date_Register", ResourceType = typeof(Caption))]
        public string Date_Register { get; set; }

        //ارتباط جداول
        [Required(ErrorMessageResourceName = "IDDocument_ERR", ErrorMessageResourceType = typeof(ErrMessage))]
        [Display(Name = "DocumentID", ResourceType = typeof(Caption))]
        public Int64 DocumentID { get; set; }
        public virtual Document Document { get; set; }


        //نحوه نمایش
        public string DisplayAttTitle
        {
            get
            {
                string res = MyClasses.Util.SubString(AttTitle, 80);
                return res;
            }
        }
    }
}
