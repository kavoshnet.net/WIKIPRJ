﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace MyClasses
{
    public static class PersianDate
    {
        private static PersianCalendar pc = new PersianCalendar();
        static PersianDate()
        {

        }
        public static string Now()
        {
            return 
                string.Format("{0}/{1}/{2}", 
                pc.GetYear(DateTime.Now).ToString("0000", CultureInfo.InvariantCulture),
                pc.GetMonth(DateTime.Now).ToString("00", CultureInfo.InvariantCulture),
                pc.GetDayOfMonth(DateTime.Now).ToString("00", CultureInfo.InvariantCulture));
        }
    }
}
